import React from 'react'

const Footer = () => {
    return (
      <footer className="bg-dark py-12 ">
        <div className="container mx-auto">
          <p className="text-white text-center">
            Copyright &copy; projet de synthèse 🤍 Ecomerce
          </p>
        </div>
      </footer>
    );
  };

export default Footer
